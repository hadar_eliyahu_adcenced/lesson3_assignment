#include <iostream>
#include "sqlite3.h"

using namespace std;
int main()
{
	sqlite3* db;
	char *zErrMsg = 0;
	int rc;
	sqlite3_open("FirstPart.db", &db);
	rc = sqlite3_exec(db, "CREATE table people(id INTEGER primary key AUTOINCREMENT, name varchar);", NULL, 0, &zErrMsg);
	if (rc)
		cout << zErrMsg << endl;
	rc = sqlite3_exec(db, "insert into people(name) values('amit');", NULL, 0, &zErrMsg);
	if (rc)
		cout << zErrMsg << endl;
	sqlite3_exec(db, "insert into people(name) values('harel');", NULL, 0, &zErrMsg);
	sqlite3_exec(db, "insert into people(name) values('roi');", NULL, 0, &zErrMsg);
	sqlite3_exec(db, "update people set name = 'ori' where id = last_insert_rowid();", NULL, 0, &zErrMsg);

	system("pause");
	return 0;
}
