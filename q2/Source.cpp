#include <iostream>
#include "sqlite3.h"
#include <string>
using namespace std;

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
int getBuyerBalance(void* notUsed, int argc, char** argv, char** azCol);
int getCarPrice(void* notUsed, int argc, char** argv, char** azCol);

int carPrice;
int buyerBalance;
int isAvailable;

int main()
{
	sqlite3* db;
	char *zErrMsg = 0;
	sqlite3_open("carsDealer.db", &db);
	carPurchase(7, 3, db, zErrMsg);
	sqlite3_close(db);
	system("pause");
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	bool res= false;
	int rc = sqlite3_exec(db, ("select price, available from cars where id = " + to_string(carid) + ";").c_str(), getCarPrice, 0, &zErrMsg);
	if (rc)
	{
		cout << zErrMsg;
	}
	else
	{
		rc = sqlite3_exec(db, ("select balnce from accounts where id = " + to_string(buyerid) + ";").c_str(), getBuyerBalance, 0, &zErrMsg);
		if (rc)
		{
			cout << zErrMsg;
		}
		else if ((carPrice > buyerBalance) && (isAvailable = 1))
		{
			rc = sqlite3_exec(db, ("update accounts set balance = " + to_string(buyerBalance - carPrice) + " where id = " + to_string(buyerid) + ";").c_str, 0, 0, &zErrMsg);
			rc = sqlite3_exec(db, ("update cars set available = 0 where id = " + to_string(carid) + ";").c_str, 0, 0, &zErrMsg);
			res = true;
		}

	}
	

	return res;
}

int getCarPrice(void* notUsed, int argc, char** argv, char** azCol)
{
	carPrice = stoi(argv[0]);
	isAvailable = stoi(argv[1]);
	return 0;
}

int getBuyerBalance(void* notUsed, int argc, char** argv, char** azCol)
{
	buyerBalance = stoi(argv[0]);
	return 0;
}


bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	rc = sqlite3_exec(db, ("select balnce from accounts where id = " + to_string(from) + ";").c_str(), getBuyerBalance, 0, &zErrMsg);
	if (rc)
	{
		cout << zErrMsg;
	}
	else if (buyerBalance >= amount)
	{
		rc = sqlite3_exec(db, ("update accounts set balance = balance + " + to_string(amount) + " where id = " + to_string(from) + ";").c_str(), 0, 0, &zErrMsg);
		if (rc)
		{
			cout << zErrMsg;
		}
		else
		{
			rc = sqlite3_exec(db, ("update accounts set balance = balance - " + to_string(amount) + " where id = " + to_string(to) + ";").c_str(), 0, 0, &zErrMsg);
			if (rc)
			{
				cout << zErrMsg;
			}
		}
		
	}
}